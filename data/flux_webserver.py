from flask import Flask, render_template, redirect, request
from fluxclient.robot import FluxRobot
from fluxclient.commands.misc import get_or_create_default_key
from tinydb import TinyDB, Query
import logging
import os

# Get flux IP from environment file
flux_ip = os.environ['FLUX_IP']
# Load RSA key
client_key = get_or_create_default_key("./flux.pem")

# Set log level
log = logging.getLogger('werkzeug')
#log.setLevel(logging.ERROR)

server = Flask(__name__)

flux_realtime = None
robot = None

@server.route("/")
@server.route("/<plain>")
def home(plain=None):

    try:
        db = TinyDB('db.json')
        Stats = Query()
        data = db.search(Stats.key == 'stats')
        total_minutes = data[0].get('total_minutes', 0)
        total_hours = float("{0:.2f}".format(total_minutes/60))

        minutes_since_lubrication = data[0].get('minutes_since_lubrication', 0)
        hours_since_lubrication = float("{0:.2f}".format(minutes_since_lubrication/60))
        status = data[0].get('status', 'UNKNOWN')

        if plain is not None:
            return render_template('plain.html', status=status, total_hours=total_hours, hours_since_lubrication=hours_since_lubrication)

        return render_template('index.html', status=status, total_hours=total_hours, hours_since_lubrication=hours_since_lubrication)

    except Exception as e:
        print(e)
        returnString = "ERROR<br/>"
        returnString += str(e)
        return returnString

def onExit():
    global flux_realtime
    global robot
    flux_realtime = None
    robot = None

@server.route("/control", methods=['GET', 'POST'])
def control():

    global robot
    global flux_realtime

    if request.method == 'GET':
        return render_template('control.html')
    else:

        try:
            command = request.args.get('command', '')

            if(command=='connect'):
                if flux_realtime:
                    flux_realtime.close()
                robot = FluxRobot((flux_ip, 23811), client_key)
                flux_realtime = robot.icontrol()
                flux_realtime.exit_callback = onExit
                flux_realtime.blocking_flag=False
                flux_realtime.home()
                return 'connected'
            elif(command=='disconnect' or not flux_realtime):
                if(flux_realtime):
                    flux_realtime.close()
                else:
                    robot = None
                return "disconnected"

            if(command=='temp'):
                try:
                    temp = request.args.get('temp', '0')
                    flux_realtime.set_temp(temp)
                    return "new temperature target: "+temp
                except Exception as e:
                    print(e)
                    return "error: could not set temperature"

            elif(command=='home'):
                pos = flux_realtime.get_position()
                flux_realtime.move(-1*pos[0], -1*pos[1], -1*pos[2]+240, relative=True)
                flux_realtime.home()

            else:
                pos = flux_realtime.get_position()

                if (command == 'z-up'):
                    if(pos[2]>=220):
                        return "not allowed to prevent damage"
                    flux_realtime.move(0, 0, 20, relative=True)
                elif (command == 'z-down'):
                    if (pos[2] <= 0):
                        return "error: invalid path"
                    flux_realtime.move(0, 0, -20, relative=True)
                else:

                    if(pos[2]>225):
                        return "not allowed to prevent damage"
                    elif(command=='x-up'):
                        if (pos[0] >= 60):
                            return "error: invalid path"
                        flux_realtime.move(20,0,0, relative=True)
                    elif(command=='x-down'):
                        if (pos[0] <= -60):
                            return "error: invalid path"
                        flux_realtime.move(-20,0,0, relative=True)
                    elif(command=='y-up'):
                        if (pos[1] >= 60):
                            return "error: invalid path"
                        flux_realtime.move(0,20,0, relative=True)
                    elif(command=='y-down'):
                        if (pos[1] <= -60):
                            return "error: invalid path"
                        flux_realtime.move(0,-20,0, relative=True)

            pos = flux_realtime.get_position()
            return "X:"+str(pos[0])+"  Y:"+str(pos[1])+"  Z:"+str(pos[2])

        except Exception as e:
            if flux_realtime:
                flux_realtime.close()
            else:
                robot = None
            print (e)
            return e.args[0]


@server.route("/reset")
def reset():
    db = TinyDB('db.json')
    Stats = Query()
    db.update({'minutes_since_lubrication': 0}, Stats.key == 'stats')
    return redirect("/", code=302)


if __name__ == "__main__":
    server.run(host='0.0.0.0', port=5000)
