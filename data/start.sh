#!/bin/bash


if python3 flux_authorize.py; then
    python3 monitor_flux.py &
    sleep 1
    python3 flux_webserver.py
fi
