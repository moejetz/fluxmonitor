from fluxclient.commands.misc import get_or_create_default_key
from fluxclient.robot import FluxRobot
from tinydb import TinyDB, Query
from tinydb.operations import increment
import time
import os

# Get flux IP from environment file
flux_ip = os.environ['FLUX_IP']

# Load RSA key
client_key = get_or_create_default_key("./flux.pem")

# Open or create database
db = TinyDB('db.json')
Stats = Query()

# If db just created, initialize structure
if(db.all()==[]):
    db.insert({'key': 'stats', 'total_minutes': 0, 'minutes_since_lubrication': 0, 'status': 'UNKNOWN'})


while True:

    try:
        # Connect to Flux Delta
        print("connecting to "+flux_ip+"...")
        robot = FluxRobot((flux_ip, 23811), client_key)
        print ("connected.")

        # Periodically check for toolhead status
        while True:

            label = robot.report_play()["st_label"]
            print("FLUX status: "+label)
            db.update({'status': label}, Stats.key == 'stats')
            if label=="RUNNING":
                db.update(increment('total_minutes'), Stats.key == 'stats')
                db.update(increment('minutes_since_lubrication'), Stats.key == 'stats')

            time.sleep(60)

    except Exception as e:

        db.update({'status': 'UNKNOWN'}, Stats.key == 'stats')
        try:
            print(e.args[1])
        except Exception as e1:
            print ("connection error")

        print("next reconnect in 20 seconds.")
        time.sleep(20)
