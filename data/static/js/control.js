window.onbeforeunload = function(){
  sendAjaxPost("control?command=disconnect");
};


var elements = document.getElementsByClassName('button-control')
for(var i=0; i<elements.length; i++) {
    elements[i].addEventListener("click", function(){
        sendAjaxPost("control?command="+this.id);
    });
}

/*
document.getElementById('set_temp').addEventListener("click", function() {
    sendAjaxPost("control?command=temp");
})
*/

function sendAjaxPost(url) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        console.log(this.responseText);
        document.getElementById('status').innerHTML = "Status: "+this.responseText;
    }
  };
  xhttp.open("POST", url, true);
  xhttp.send();
}