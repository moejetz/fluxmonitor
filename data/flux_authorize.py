import sys
import os
from fluxclient.upnp import UpnpDiscover
from fluxclient.upnp import discover_device
from fluxclient.commands.misc import get_or_create_default_key


# Get environment variables
flux_ip = os.environ['FLUX_IP']
flux_password = os.environ['FLUX_PASSWORD']

flux_key = get_or_create_default_key("./flux.pem")

def deviceDiscovered(upnp_discover, device, **kw):
    print("device '%s' found at %s" % (device.name, device.ipaddr))

    try:
        # Test if the machine authorized
        upnp_task = device.manage_device(flux_key)
        print ("checking authorisation...")

        if upnp_task.authorized:
            print("key authorized.")
        else:
            print("key not authorized. authorizing...")
            upnp_task.authorize_with_password(flux_password)
            upnp_task.add_trust("flux_monitor", flux_key.public_key_pem.decode())
            print("authorized")

    except Exception as e:
        print("authorization failed.")
        print (e)
        sys.exit(1)


    # Find only one printer in this example
    upnp_discover.stop()


print("discovering device %s..." % (flux_ip))
upnp_discover = UpnpDiscover(None, flux_ip)
upnp_discover.discover(deviceDiscovered, None, 10)






