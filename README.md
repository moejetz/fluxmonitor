# FLUX Monitor
This is a monitoring tool for the Flux Delta 3D printer family ([flux3dp.com](http://flux3dp.com)).

Its main purpose is to sum up the running time of the printer to give the user an idea about when it's time to lubricate.

So what it does is it checks once a minute if the printer is running, and if so, add a minute to a counter.

The data can be viewed over a web interface.

### Setup ###
* Clone the repository
* Install and run Docker and Docker Compose
* Copy the file ```data/variables_template.env``` to ```data/variables.env```
* Adjust the variables in that file
	* IP-address of the printer
	* The printer password (the one you entered in Flux Studio)
* Go to the repository root folder (fluxmonitor)
* At this point, make sure the printer is turned on and the values in ```data/variables.env``` are correct!
* Run ```docker-compose up``` to start the server
* Use a webbrowser to connect to the server's IP-address, for example ```http://192.168.0.50```
* See the stats.
* To stop the server, run ```docker-compose stop```
* To start the server in daemon mode, run ```docker-compose up -d```


### Usage ###
* Print a lot of cool stuff!
* Lubricate after max. 50 hours of running time
* Reset the counter
* Print even more cool stuff!

### Update ###
To install updates, just go to the root folder (fluxmonitor) and run the following commands:

* ```git pull origin master```
* ```docker-compose stop```
* ```docker-compose up -d```

### Note ###
The printer password is only needed the first time to add the host machine's RSA key to the printer.

After that, the host machine authorizes with this RSA key.

So if you don't like having plain text passwords laying around anywhere, feel free to replace it inside the variables.env file once the host machine was authorized!

### License ###
```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
